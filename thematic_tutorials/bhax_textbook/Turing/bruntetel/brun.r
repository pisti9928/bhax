ikerprimosszege <- function(x)
{
  prim = primes(x)
  kul = prim[2:length(prim)]-prim[1:length(prim)-1]
  ikph = which(kul==2)
  ikp1 = prim[ikph]
  ikp2 = prim[ikph]+2
  rpikp12 = 1/ikp1 + 1/ikp2
  return(sum(rpikp12))
}
  