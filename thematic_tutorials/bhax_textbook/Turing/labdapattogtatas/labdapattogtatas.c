#include <stdio.h>
#include <curses.h>
#include <unistd.h>
int main (void)
{
    WINDOW *ablak;
    ablak = initscr ();
    int x = 0; //oszlop
    int y = 0; //sor
    int mx = 1; //módosítás értéke
    int my = 1; //módosítás értéke
    int sorok_szama;
    int oszlopok_szama;
    while(TRUE)
    {
        getmaxyx(ablak,sorok_szama,oszlopok_szama);
        mvprintw(y,x,"0");
        refresh();
        usleep (100000);
        clear();
        x+=mx;
        y+=my;
        if(x<=0) //bal oszlop
        {
            mx*=-1;
        }
        if(x>=oszlopok_szama-1) //jobb oszlop
        {
            mx*=-1;
        }
        if(y<=0) //felso sor
        {
            my*=-1;
        }
        if(y>=sorok_szama-1) //also sor
        {
            my*=-1;
        }
    }
}
