from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

# Added modifications by Norbert Bátfai (nb4tf4i) batfai.norbert@inf.unideb.hu, mine.ly/nb4tf4i.1
# 2018.10.18, https://bhaxor.blog.hu/2018/10/18/malmo_minecraft
# 2020.02.02, NB4tf4i's Red Flowers, http://smartcity.inf.unideb.hu/~norbi/NB4tf4iRedFlowerHell


from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math

if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools

    print = functools.partial(print, flush=True)

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse(sys.argv)
except RuntimeError as e:
    print('ERROR:', e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #

missionXML_file = 'nb4tf4i_d7x7x7.xml'
with open(missionXML_file, 'r') as f:
    print("NB4tf4i's Red Flowers (Red Flower Hell) - DEAC-Hackers Battle Royale Arena\n")
    print("NB4tf4i vörös pipacsai (Vörös Pipacs Pokol) - DEAC-Hackers Battle Royale Arena\n\n")
    print(
        "The aim of this first challenge, called nb4tf4i's red flowers, is to collect as many red flowers as possible before the lava flows down the hillside.\n")
    print(
        "Ennek az első, az nb4tf4i vörös virágai nevű kihívásnak a célja összegyűjteni annyi piros virágot, amennyit csak lehet, mielőtt a láva lefolyik a hegyoldalon.\n")
    print("Norbert Bátfai, batfai.norbert@inf.unideb.hu, https://arato.inf.unideb.hu/batfai.norbert/\n\n")
    print("Loading mission from %s" % missionXML_file)
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock(0, 0, 0, "lava")


class Hourglass:
    def __init__(self, charSet):
        self.charSet = charSet
        self.index = 0

    def cursor(self):
        self.index = (self.index + 1) % len(self.charSet)
        return self.charSet[self.index]


hg = Hourglass('|/-\|')
# classon belul hogy? pls
turn_count = 0


class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        self.x = 0
        self.y = 0
        self.z = 0
        self.x_old = 0
        self.y_old = 0
        self.z_old = 0
        self.yaw = 0
        self.pitch = 0
        self.lookingat = ""
        self.turncount = 0
        self.kettotle = False
        self.egyetle = False
        self.harmatle = False
        self.negyetle = False
        self.hotbar = True
        self.poppy = 0
        self.poppycheck = 0

    def simanfel(self, ut):
        self.agent_host.sendCommand("turn 1")
        self.agent_host.sendCommand("turn 1")
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")

        roadfel = 0
        while (roadfel < ut):
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("jumpmove 1")
            roadfel += 1
            time.sleep(.1)  # .2   .05
        # self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("turn -1")
        time.sleep(.01)  # .2

    def lavaig(self):
        lavatour = True
        """roadfel = 0
        while(roadfel < 48):
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("jumpmove 1")
            roadfel += 1
            """
        while lavatour:
            world_state = self.agent_host.getWorldState()
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                observations = json.loads(sensations)
                nbr7x7x7 = observations.get("nbr7x7", 0)
                # print("    3x3x3 neighborhood of Steve: ", nbr7x7x7)
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y = int(observations["YPos"])
                # print("    Steve's Coords: ", self.x, self.y, self.z)
                # print("    Steve's Yaw: ", self.yaw)
                # print("    Steve's Pitch: ", self.pitch)
                if ("flowing_lava" in nbr7x7x7):
                    lavatour = False
                    self.agent_host.sendCommand("move -1")
                    self.agent_host.sendCommand("move -1")
                    self.agent_host.sendCommand("turn 1")
            if (lavatour):
                self.agent_host.sendCommand("move 1")
                self.agent_host.sendCommand("jumpmove 1")
            time.sleep(.05)
        print("Lávát látok")

    def lavatol_vissza(self, emelet):
        self.agent_host.sendCommand("turn 1")
        akt_emelet = 0
        while akt_emelet < emelet:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            akt_emelet += 1
        time.sleep(.1)
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("turn 1")
        print(self.yaw)
        time.sleep(.05)

    def is_lava(self, nbr7x7x7):
        if "flowing_lava" in nbr7x7x7:
            if (self.kettotle):
                self.szint_2()
            else:
                self.szint_1()

    def go_down(self):
        if self.negyetle and self.harmatle and self.kettotle and self.egyetle:
            self.szint_4()
            self.turncount = 0
        elif self.harmatle and self.kettotle and self.egyetle:
            self.szint_3()
            self.turncount = 0
        elif self.egyetle and self.kettotle:
            self.szint_2()
            self.turncount = 0
        elif self.egyetle:
            self.szint_1()
            self.turncount = 0

    def szint_1(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        #print("1")
        self.egyetle = self.kettotle
        self.kettotle = self.harmatle
        self.harmatle = self.negyetle
        self.negyetle = False

    def szint_2(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        #print("2")
        self.egyetle = self.harmatle
        self.kettotle = self.negyetle
        self.harmatle = False
        self.negyetle = False

    def szint_3(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        #print("3")
        self.egyetle = self.negyetle
        self.kettotle = False
        self.harmatle = False
        self.negyetle = False

    def szint_4(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        #print("4")
        self.egyetle = False
        self.kettotle = False
        self.harmatle = False
        self.negyetle = False

    def hunavirag(self, jje3, jje2, jje1, jj, jjh1, jjh2, jjh3, je3, je2, je1, j, jh1, jh2, jh3, be3, be2, be1, b, bh1, bh2, bh3, bbe3, bbe2, bbe1, bb, bbh1, bbh2, bbh3,nbr):
        if (nbr[jje3] == "red_flower"):
            self.right_right_flower(3)
        if (nbr[jje2] == "red_flower"):
            self.right_right_flower(2)
        if (nbr[jje1] == "red_flower"):
            self.right_right_flower(1)
        if (nbr[jj] == "red_flower"):
            self.right_right_flower(0)
        if (nbr[jjh1] == "red_flower"):
            self.right_right_flower(-1)
        if (nbr[jjh2] == "red_flower"):
            self.right_right_flower(-2)
        if (nbr[jjh3] == "red_flower"):
            self.right_right_flower(-3)
        if (nbr[je3] == "red_flower"):
            self.right_flower(3)
        if (nbr[je2] == "red_flower"):
            self.right_flower(2)
        if (nbr[je1] == "red_flower"):
            self.right_flower(1)
        if (nbr[j] == "red_flower"):
            self.right_flower(0)
        if (nbr[jh1] == "red_flower"):
            self.right_flower(-1)
        if (nbr[jh2] == "red_flower"):
            self.right_flower(-2)
        if (nbr[jh3] == "red_flower"):
            self.right_flower(-3)
        if (nbr[be3] == "red_flower"):
            self.left_flower(3)
        if (nbr[be2] == "red_flower"):
            self.left_flower(2)
        if (nbr[be1] == "red_flower"):
            self.left_flower(1)
        if (nbr[b] == "red_flower"):
            self.left_flower(0)
        if (nbr[bh1] == "red_flower"):
            self.left_flower(-1)
        if (nbr[bh2] == "red_flower"):
            self.left_flower(-2)
        if (nbr[bh3] == "red_flower"):
            self.left_flower(-3)
        if (nbr[bbe3] == "red_flower"):
            self.left_left_flower(3)
        if (nbr[bbe2] == "red_flower"):
            self.left_left_flower(2)
        if (nbr[bbe1] == "red_flower"):
            self.left_left_flower(1)
        if (nbr[bb] == "red_flower"):
            self.left_left_flower(0)
        if (nbr[bbh1] == "red_flower"):
            self.left_left_flower(-1)
        if (nbr[bbh2] == "red_flower"):
            self.left_left_flower(-2)
        if (nbr[bbh3] == "red_flower"):
            self.left_left_flower(-3)
        self.go_down()

    def is_flower(self, nbr):
        if (self.yaw == 0):
            self.hunavirag(91,84,77,70,63,56,49,191,184,177,170,163,156,149,242,235,228,221,214,207,200,293,286,279,272,265,258,251, nbr)
        elif (self.yaw == 90):
            self.hunavirag(196,197,198,199,200,201,202,161,162,163,164,165,166,167,126,127,128,129,130,131,132,91,92,93,94,95,96,97 ,nbr)
        elif (self.yaw == 180):
            self.hunavirag(202,209,216,223,230,237,244,151,158,165,172,179,186,193,100,107,114,121,128,135,142,49,56,63,70,77,84,91 ,nbr)
        else:
            self.hunavirag(146,145,144,143,142,141,140,181,180,179,178,177,176,175,216,215,214,213,212,211,210,251,250,249,248,247,246,245 ,nbr)

    def center_flower(self, mezo):
        # print("x: "+str(self.x) + " old_ "+ str(self.x_old) +"     z:  "+str(self.z)+"  old_ "+str(self.z_old))

        """if(abs(self.x)==abs(self.x_old)-5 or abs(self.z) ==abs(self.z_old)-5):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif(abs(self.x)==abs(self.x_old)-4 or abs(self.z) == abs(self.z_old)-4):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif(abs(self.x)==abs(self.x_old)-3 or abs(self.z) == abs(self.z_old)-3):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 2 or abs(self.z) == abs(self.z_old) - 2):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 1 or abs(self.z) == abs(self.z_old) - 1):
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)"""
        if mezo == -3:
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
        elif mezo == -2:
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
        elif mezo == -1:
            self.agent_host.sendCommand("strafe 1")
        elif mezo == 1:
            self.agent_host.sendCommand("strafe -1")
        elif mezo == 2:
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
        elif mezo == 3:
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
        time.sleep(.05)
        self.get_flower()
        self.harmatle = True
        if mezo == -3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == 3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)
    def left_left_flower(self,mezo):
        if mezo == -3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == 3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        #self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.05)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)  # .2
        self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.negyetle = True
        if mezo == -3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == 3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")

        time.sleep(0.05)

    def left_flower(self, mezo):
        if mezo == -3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")

        elif mezo == -1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == 3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        #self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.05)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.05)  # .2
        #self.agent_host.sendCommand("move 1")
        # time.sleep(.1)
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.harmatle = True
        if mezo == -3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == 3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")

        time.sleep(0.05)

    def right_flower(self, mezo):
        self.agent_host.sendCommand("strafe 1")
        if mezo == -3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")

        elif mezo == -1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == 3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        self.get_flower()
        self.agent_host.sendCommand("strafe -1")
        self.kettotle = True
        if mezo == -3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == 3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)

    def right_right_flower(self, mezo):
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)
        self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.05)
        self.agent_host.sendCommand("move 1")
        time.sleep(.05)

        time.sleep(.05)
        if mezo == -3:
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
        elif mezo == -2:
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
        elif mezo == -1:
            self.agent_host.sendCommand("strafe 1")
        elif mezo == 1:
            self.agent_host.sendCommand("strafe -1")
        elif mezo == 2:
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
        elif mezo == 3:
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        self.get_flower()
        self.agent_host.sendCommand("move -1")
        time.sleep(.05)
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("move -1")
        time.sleep(.05)
        self.agent_host.sendCommand("turn -1")
        time.sleep(.01)
        self.egyetle = True
        if mezo == -3:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == 3:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)

    """
        
    def right_flower(self,nbr7x7x7):
        #if (nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)    #.05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_e1(self,nbr7x7x7):
        #if (nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)
        #self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)    #.05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_h1(self, nbr7x7x7):
        # if (nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)  # .05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            # print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            # self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  # .1
            self.kettotle = False
        else:
            # print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  # .1
            self.kettotle = False

    def left_flower(self,nbr7x7x7):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)            #.1
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)      #.1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)    #.1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)      #.2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)     #.1
        self.kettotle=True

    def left_flower_e1(self, nbr7x7x7):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)  # .1
        #self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)  # .2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.kettotle = True

    def left_flower_h1(self, nbr7x7x7):
        if ((nbr7x7x7[10] != "dirt" and self.yaw == 180) or (nbr7x7x7[12] != "dirt" and self.yaw == 90) or (nbr7x7x7[14] != "dirt" and self.yaw == 270) or (nbr7x7x7[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)  # .1
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)  # .2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.kettotle = True

    def alatta_flower(self, nbr7x7x7):
        steve.get_flower()
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False
    def elotte_flower(self, nbr7x7x7):
        self.agent_host.sendCommand("move 1")
        steve.get_flower()
        self.agent_host.sendCommand("move -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False
    """

    def is_wall(self, nbr7x7x7):
        world_state = self.agent_host.getWorldState()

        if (self.yaw == 0):
            if (nbr7x7x7[178] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    if(self.y>=12):
                            self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0
                    # print("lentebb")

        elif (self.yaw == 270):
            if (nbr7x7x7[172] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0

        elif (self.yaw == 180):
            if (nbr7x7x7[164] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0

        else:
            if (nbr7x7x7[170] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0
        world_state = self.agent_host.getWorldState()
        while world_state.number_of_observations_since_last_state == 0:
            world_state = self.agent_host.getWorldState()
            pass
        sensations = world_state.observations[-1].text
        # print("    sensations: ", sensations)
        observations = json.loads(sensations)
        nbr7x7x7 = observations.get("nbr7x7", 0)
        # print("\n    3x3x3 neighborhood of Steve: \n", nbr7x7x7)

        if "Yaw" in observations:
            self.yaw = int(observations["Yaw"])
        if "Pitch" in observations:
            self.pitch = int(observations["Pitch"])
        if "XPos" in observations:
            self.x = int(observations["XPos"])
        if "ZPos" in observations:
            self.z = int(observations["ZPos"])
        if "YPos" in observations:
            self.y = int(observations["YPos"])
        self.is_flower(nbr7x7x7)

    def get_flower(self):
        print("red_flower")
        self.agent_host.sendCommand("attack 1")
        time.sleep(.4)  # .5   #.3
        world_state = self.agent_host.getWorldState()
        sensations = world_state.observations[-1].text
        observations = json.loads(sensations)
        hotbar0 = ""
        # hotbar1=""
        if "Hotbar_0_variant" in observations:
            hotbar0 = observations["Hotbar_0_variant"]
        # if"Hotbar_1_variant" in observations:
        #    hotbar1 = observations["Hotbar_1_variant"]
        # print(hotbar0)
        # print(hotbar1)
        if hotbar0 == "poppy" and self.hotbar:
            self.agent_host.sendCommand("hotbar.2 1")
            self.agent_host.sendCommand("hotbar.2 0")
            time.sleep(.1)
            self.hotbar = False
        """else:
            if"Hotbar_1_size" in observations:
                self.poppy = observations["Hotbar_1_size"]
        self.poppycheck+=1
        if(self.poppycheck != self.poppy):
            world_state = self.agent_host.getWorldState()
            while world_state.number_of_observations_since_last_state == 0:
                world_state = self.agent_host.getWorldState()
                pass
            sensations = world_state.observations[-1].text
            # print("    sensations: ", sensations)
            observations = json.loads(sensations)
            nbr7x7x7 = observations.get("nbr7x7", 0)
            # print("\n    3x3x3 neighborhood of Steve: \n", nbr7x7x7)

            if "Yaw" in observations:
                self.yaw = int(observations["Yaw"])
            if "Pitch" in observations:
                self.pitch = int(observations["Pitch"])
            if "XPos" in observations:
                self.x = int(observations["XPos"])
            if "ZPos" in observations:
                self.z = int(observations["ZPos"])
            if "YPos" in observations:
                self.y = int(observations["YPos"])
            self.is_flower(nbr7x7x7)
        # time.sleep(.05)  #.1
        """
        self.agent_host.sendCommand("jumpuse")
        time.sleep(.1)  # .2
        # else:
        # time.sleep(.1)
        """self.agent_host.sendCommand("strafe -1")  # visszamegy az elozo helyere
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")  # 1 szinttel lentebb
        self.agent_host.sendCommand("move -1")  # sarok miatt
        self.agent_host.sendCommand("move -1")  #
        """

        time.sleep(.1)

    def move(self, nbr7x7x7):

        if self.y>=12:
            steve.is_lava(nbr7x7x7)
            steve.is_flower(nbr7x7x7)
            steve.is_wall(nbr7x7x7)
        """if(self.y==4 or self.y==3):
            self.agent_host.sendCommand("move 1")
        else:"""
        #self.agent_host.sendCommand("strafe 1")
        if self.y<20:
            if self.y<12:
                self.harmatle=False
                self.kettotle=False
                self.negyetle=False
                if(nbr7x7x7[171]=="red_flower"):
                    self.get_flower()
                    self.agent_host.sendCommand("strafe -1")
                    self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0
                self.is_wall(nbr7x7x7)
                self.agent_host.sendCommand("strafe 1")
                self.agent_host.sendCommand("move 1")
                time.sleep(.1)
            else:
                self.agent_host.sendCommand("move 1")
                self.agent_host.sendCommand("move 1")
                self.agent_host.sendCommand("move 1")
                time.sleep(.1)
        else:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            time.sleep(.1)
        # world_state = self.agent_host.getWorldState()

    def run(self):
        world_state = self.agent_host.getWorldState()
        # world_state = self.agent_host.peekWorldState()
        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("look 1")
        self.hotbar = True
        # Loop until mission ends:
        # steve.simanfel(60)  #35-->31    40-->37    43nal meghal!! #41-->37    !!!!!!47!!!!   #50 --> 46
        steve.lavaig()
        steve.lavatol_vissza(10)  # 33-->23
        self.agent_host.sendCommand("strafe 1")
        self.agent_host.sendCommand("strafe 1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.05)
        while world_state.is_mission_running:
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                # print("    sensations: ", sensations)
                observations = json.loads(sensations)
                nbr7x7x7 = observations.get("nbr7x7", 0)
                # print("\n    3x3x3 neighborhood of Steve: \n", nbr7x7x7)

                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y = int(observations["YPos"])
                # print(self.yaw)
                steve.move(nbr7x7x7)
            # else:
            # print("MOREEBUUBAJLESZ")

            world_state = self.agent_host.getWorldState()


num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission(my_mission, my_mission_record)
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start ")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        print("\r" + hg.cursor(), end="")
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:", error.text)

    print("NB4tf4i Red Flower Hell running\n")
    steve = Steve(agent_host)
    steve.run()

print("Mission ended")
# Mission has ended.
