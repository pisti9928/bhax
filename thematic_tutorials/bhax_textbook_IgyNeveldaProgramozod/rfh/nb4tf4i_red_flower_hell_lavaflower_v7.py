from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

# Added modifications by Norbert Bátfai (nb4tf4i) batfai.norbert@inf.unideb.hu, mine.ly/nb4tf4i.1
# 2018.10.18, https://bhaxor.blog.hu/2018/10/18/malmo_minecraft
# 2020.02.02, NB4tf4i's Red Flowers, http://smartcity.inf.unideb.hu/~norbi/NB4tf4iRedFlowerHell


from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math


if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools

    print = functools.partial(print, flush=True)

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse(sys.argv)
except RuntimeError as e:
    print('ERROR:', e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #

missionXML_file = 'nb4tf4i_d.xml'
with open(missionXML_file, 'r') as f:
    print("NB4tf4i's Red Flowers (Red Flower Hell) - DEAC-Hackers Battle Royale Arena\n")
    print("NB4tf4i vörös pipacsai (Vörös Pipacs Pokol) - DEAC-Hackers Battle Royale Arena\n\n")
    print(
        "The aim of this first challenge, called nb4tf4i's red flowers, is to collect as many red flowers as possible before the lava flows down the hillside.\n")
    print(
        "Ennek az első, az nb4tf4i vörös virágai nevű kihívásnak a célja összegyűjteni annyi piros virágot, amennyit csak lehet, mielőtt a láva lefolyik a hegyoldalon.\n")
    print("Norbert Bátfai, batfai.norbert@inf.unideb.hu, https://arato.inf.unideb.hu/batfai.norbert/\n\n")
    print("Loading mission from %s" % missionXML_file)
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock(0, 0, 0, "lava")


class Hourglass:
    def __init__(self, charSet):
        self.charSet = charSet
        self.index = 0

    def cursor(self):
        self.index = (self.index + 1) % len(self.charSet)
        return self.charSet[self.index]


hg = Hourglass('|/-\|')
#classon belul hogy? pls
turn_count = 0
class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        self.x = 0
        self.y = 0
        self.z = 0
        self.yaw = 0
        self.pitch = 0
        self.lookingat = ""
        self.turncount = 0
        self.kettotle=False
        self.hotbar=True

    def movee_1(self):
        self.agent_host.sendCommand("move 1")
    def moveh_1(self):
        self.agent_host.sendCommand("move -1")
    def jumpmovee_1(self):
        self.agent_host.sendCommand("jumpmove 1")
    def turnj_1(self):
        self.agent_host.sendCommand("turn 1")
    def turnb_1(self):
        self.agent_host.sendCommand("turn -1")
    def attack_1(self):
        self.agent_host.sendCommand("attack 1")
    def strafeb_1(self):
        self.agent_host.sendCommand("strafe -1")
    def strafej_1(self):
        self.agent_host.sendCommand("strafe 1")
    def hotbar_1(self):
        self.agent_host.sendCommand("hotbar.2 1")
    def hotbar_2(self):
        self.agent_host.sendCommand("hotbar.2 0")
    def look_1(self):
        self.agent_host.sendCommand("look 1")
    def jumpuse(self):
        self.agent_host.sendCommand("jumpuse")

    def simanfel(self, ut):
        self.turnj_1()
        self.turnj_1()
        self.movee_1()
        self.movee_1()

        roadfel = 0
        while (roadfel < ut):
            self.movee_1()
            self.jumpmovee_1()
            roadfel += 1
            #time.sleep(.1)      #.2   .05
        #self.movee_1()
        self.turnb_1()
        #time.sleep(.01)     #.2
    def lavaig(self):
        lavatour=True
        """roadfel = 0
        while(roadfel < 48):
            self.movee_1()
            self.jumpmovee_1()
            roadfel += 1
            """
        while lavatour:
            world_state = self.agent_host.getWorldState()
            time.sleep(0.05)
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                observations = json.loads(sensations)
                nbr3x3x3 = observations.get("nbr3x3", 0)
                # print("    3x3x3 neighborhood of Steve: ", nbr3x3x3)
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y = int(observations["YPos"])
                # print("    Steve's Coords: ", self.x, self.y, self.z)
                # print("    Steve's Yaw: ", self.yaw)
                # print("    Steve's Pitch: ", self.pitch)
                if ("flowing_lava" in nbr3x3x3 ):
                    lavatour = False
                    self.moveh_1()
                    self.moveh_1()
                    self.turnj_1()
            if (lavatour):
                self.movee_1()
                self.jumpmovee_1()

        print("Lávát látok")
    def lavatol_vissza(self,emelet):
        self.turnj_1()
        akt_emelet=0
        while akt_emelet<emelet:
            self.movee_1()
            self.movee_1()
            akt_emelet+=1
        #time.sleep(.1)
        self.moveh_1()
        self.moveh_1()
        #time.sleep(.1)
        self.turnj_1()
        print(self.yaw)
        #time.sleep(.05)

    def is_lava(self,nbr3x3x3):
        if "flowing_lava" in nbr3x3x3:
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            """self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1"""
            self.kettotle=False
            #time.sleep(.1)

    def is_flower(self, nbr3x3x3):
        if(self.yaw==0):
            if(nbr3x3x3[12]=="red_flower"):
                #time.sleep(.05)                #.1
                steve.right_flower(nbr3x3x3)
                self.turncount = 0
            elif(nbr3x3x3[5]=="red_flower"):
                #time.sleep(.05)
                steve.left_flower(nbr3x3x3)
                #self.turncount = 0
            elif(nbr3x3x3[8]=="red_flower"):
                #time.sleep(.05)
                steve.left_flower_e1(nbr3x3x3)
                #self.turncount = 0
            elif(nbr3x3x3[2]=="red_flower"):
                #time.sleep(.05)
                steve.left_flower_h1(nbr3x3x3)
                #self.turncount = 0
            elif(nbr3x3x3[15]=="red_flower"):
                #time.sleep(.05)
                steve.right_flower_e1(nbr3x3x3)
                self.turncount = 0
            elif(nbr3x3x3[9]=="red_flower"):
                #time.sleep(.05)
                steve.right_flower_h1(nbr3x3x3)
                self.turncount = 0
            elif(nbr3x3x3[13]=="red_flower"):
                #time.sleep(.05)
                steve.alatta_flower(nbr3x3x3)
                self.turncount=0
            elif(nbr3x3x3[16]=="red_flower"):
                #time.sleep(.05)
                steve.elotte_flower(nbr3x3x3)
                self.turncount = 0
        elif(self.yaw==90):
            if (nbr3x3x3[10] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[7] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[6] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_e1(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[8] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_h1(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[9] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_e1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[11] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_h1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[13] == "red_flower"):
                #time.sleep(.05)
                steve.alatta_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[12] == "red_flower"):
                #time.sleep(.05)
                steve.elotte_flower(nbr3x3x3)
                self.turncount = 0
        elif(self.yaw==180):
            if (nbr3x3x3[14] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[3] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[0] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_e1(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[6] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_h1(nbr3x3x3)
                #self.turncount = 0
            elif (nbr3x3x3[11] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_e1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[17] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_h1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[13] == "red_flower"):
                #time.sleep(.05)
                steve.alatta_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[10] == "red_flower"):
                #time.sleep(.05)
                steve.elotte_flower(nbr3x3x3)
                self.turncount = 0
        else:
            if (nbr3x3x3[16] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[1] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[2] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_e1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[0] == "red_flower"):
                #time.sleep(.05)
                steve.left_flower_h1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[17] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_e1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[15] == "red_flower"):
                #time.sleep(.05)
                steve.right_flower_h1(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[13] == "red_flower"):
                #time.sleep(.05)
                steve.alatta_flower(nbr3x3x3)
                self.turncount = 0
            elif (nbr3x3x3[14] == "red_flower"):
                #time.sleep(.05)
                steve.elotte_flower(nbr3x3x3)
                self.turncount = 0
            """if((nbr3x3x3[10]!="dirt" and self.yaw==180) or (nbr3x3x3[12]!="dirt" and self.yaw==90) or (nbr3x3x3[14]!="dirt" and self.yaw==270) or (nbr3x3x3[16]!="dirt" and self.yaw==0)):
                self.moveh_1()
            time.sleep(.1)
            steve.get_flower()"""


    def right_flower(self,nbr3x3x3):
        #if (nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.1)
        self.strafej_1()
        #time.sleep(.1)    #.05
        steve.get_flower()
        self.strafeb_1
        if self.kettotle:
            #print("kettotle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #self.strafeb_1
            #time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_e1(self,nbr3x3x3):
        #if (nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.1)
        #self.movee_1()
        #time.sleep(.1)
        self.strafej_1()
        #time.sleep(.1)    #.05
        steve.get_flower()
        self.strafeb_1
        if self.kettotle:
            #print("kettotle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #self.strafeb_1
            #time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_h1(self, nbr3x3x3):
        # if (nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.05)
        self.moveh_1()
        #time.sleep(.1)
        self.strafej_1()
        #time.sleep(.1)  # .05
        steve.get_flower()
        self.strafeb_1
        if self.kettotle:
            # print("kettotle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            # self.strafeb_1
            #time.sleep(.05)  # .1
            self.kettotle = False
        else:
            # print("egyetle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #time.sleep(.05)  # .1
            self.kettotle = False
#kommentelve a valtoztatott time.sleep!!!!
    def left_flower(self,nbr3x3x3):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.1)            #.1
        self.strafeb_1
        #time.sleep(.1)      #.1
        steve.get_flower()
        self.turnj_1()
        #time.sleep(.05)    #.1
        self.jumpmovee_1()
        #time.sleep(.1)      #.2
        self.turnb_1()
        #time.sleep(.05)     #.1
        self.kettotle=True

    def left_flower_e1(self, nbr3x3x3):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.1)  # .1
        #self.movee_1()
        #time.sleep(.1)
        self.strafeb_1
        #time.sleep(.1)  # .1
        steve.get_flower()
        self.turnj_1()
        #time.sleep(.05)  # .1
        self.jumpmovee_1()
        #time.sleep(.1)  # .2
        self.turnb_1()
        #time.sleep(.05)  # .1
        self.kettotle = True

    def left_flower_h1(self, nbr3x3x3):
        if ((nbr3x3x3[10] != "dirt" and self.yaw == 180) or (nbr3x3x3[12] != "dirt" and self.yaw == 90) or (nbr3x3x3[14] != "dirt" and self.yaw == 270) or (nbr3x3x3[16] != "dirt" and self.yaw == 0)):
            self.moveh_1()
            self.moveh_1()
            self.moveh_1()
            #time.sleep(.05)  # .1
        self.moveh_1()
        #time.sleep(.1)
        self.strafeb_1
        #time.sleep(.1)  # .1
        steve.get_flower()
        self.turnj_1()
        #time.sleep(.05)  # .1
        self.jumpmovee_1()
        #time.sleep(.1)  # .2
        self.turnb_1()
        #time.sleep(.05)  # .1
        self.kettotle = True

    def alatta_flower(self, nbr3x3x3):
        steve.get_flower()
        if self.kettotle:
            #print("kettotle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #self.strafeb_1
            #time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #time.sleep(.05)    #.1
            self.kettotle=False
    def elotte_flower(self, nbr3x3x3):
        self.movee_1()
        steve.get_flower()
        self.moveh_1()
        if self.kettotle:
            #print("kettotle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #self.strafeb_1
            #time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.moveh_1()
            self.strafeb_1
            self.moveh_1()
            self.strafeb_1
            #time.sleep(.05)    #.1
            self.kettotle=False

    def is_wall(self,nbr3x3x3):
        world_state=self.agent_host.getWorldState()

        if (self.yaw == 0):
            if (nbr3x3x3[15] == "dirt" and nbr3x3x3[16] == "dirt" and nbr3x3x3[17] == "dirt"):  #elotte fal
                if(self.turncount<=4 or self.kettotle==False):
                    self.moveh_1()
                    #time.sleep(.1)
                    self.turnb_1()
                    #time.sleep(.05)
                    self.turncount+=1
                else:
                    if self.kettotle:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    else:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    #time.sleep(.1)
                    self.turncount = 0
                    #print("lentebb")

        elif (self.yaw == 270):
            if (nbr3x3x3[11] == "dirt" and nbr3x3x3[14] == "dirt" and nbr3x3x3[17] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.kettotle==False):
                    self.moveh_1()
                    #time.sleep(.1)
                    self.turnb_1()
                    #time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    else:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    #time.sleep(.1)
                    self.turncount = 0

        elif (self.yaw == 180):
            if (nbr3x3x3[9] == "dirt" and nbr3x3x3[10] == "dirt" and nbr3x3x3[11] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.kettotle==False):
                    self.moveh_1()
                    #time.sleep(.1)
                    self.turnb_1()
                    #time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    else:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    #time.sleep(.1)
                    self.turncount = 0

        else:
            if (nbr3x3x3[9] == "dirt" and nbr3x3x3[12] == "dirt" and nbr3x3x3[15] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.kettotle==False):
                    self.moveh_1()
                    #time.sleep(.1)
                    self.turnb_1()
                    #time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    else:
                        self.moveh_1()
                        self.strafeb_1
                        self.moveh_1()
                        self.strafeb_1
                    #time.sleep(.1)
                    self.turncount = 0


    def get_flower(self):
        print("red_flower")
        self.attack_1()
        #time.sleep(.3)   #.5   #.3
        world_state = self.agent_host.getWorldState()
        sensations = world_state.observations[-1].text
        observations = json.loads(sensations)
        hotbar0=""
        #hotbar1=""
        if"Hotbar_0_variant" in observations:
            hotbar0 = observations["Hotbar_0_variant"]
        #if"Hotbar_1_variant" in observations:
        #    hotbar1 = observations["Hotbar_1_variant"]
        #print(hotbar0)
        #print(hotbar1)
        if hotbar0=="poppy" and self.hotbar:
            self.hotbar_2()
            self.hotbar_1()
            #time.sleep(.1)
            self.hotbar=False
        #if hotbar0=="dirt" or hotbar1=="dirt":
        #time.sleep(.05)  #.1
        self.jumpuse()
        #time.sleep(.1)   #.2
        #else:
        #time.sleep(.1)
        """self.strafeb_1  # visszamegy az elozo helyere
        time.sleep(.1)
        self.strafeb_1  # 1 szinttel lentebb
        self.moveh_1()  # sarok miatt
        self.moveh_1()  #
        """


        #time.sleep(.1)

    def move(self,nbr3x3x3):
        #steve.is_lava(nbr3x3x3)
        steve.is_flower(nbr3x3x3)
        steve.is_wall(nbr3x3x3)
        """if(self.y==4 or self.y==3):
            self.movee_1()
        else:"""
        self.movee_1()
        self.movee_1()
        self.movee_1()
        #time.sleep(.05)


    def run(self):
        world_state = self.agent_host.getWorldState()
        #world_state = self.agent_host.peekWorldState()
        self.look_1()
        self.look_1()
        self.hotbar=True
        # Loop until mission ends:
        #steve.simanfel(60)  #35-->31    40-->37    43nal meghal!! #41-->37    !!!!!!47!!!!   #50 --> 46
        steve.lavaig()
        steve.lavatol_vissza(15) #33-->23
        while world_state.is_mission_running:
            time.sleep(.05)
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                # print("    sensations: ", sensations)
                observations = json.loads(sensations)
                nbr3x3x3 = observations.get("nbr3x3", 0)
                # print("\n    3x3x3 neighborhood of Steve: \n", nbr3x3x3)
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y = int(observations["YPos"])
                #print(self.yaw)
                steve.move(nbr3x3x3)
            #else:
                #print("MOREEBUUBAJLESZ")

            world_state = self.agent_host.getWorldState()




num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission(my_mission, my_mission_record)
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start ")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        print("\r" + hg.cursor(), end="")
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:", error.text)

    print("NB4tf4i Red Flower Hell running\n")
    steve = Steve(agent_host)
    steve.run()

print("Mission ended")
# Mission has ended.
