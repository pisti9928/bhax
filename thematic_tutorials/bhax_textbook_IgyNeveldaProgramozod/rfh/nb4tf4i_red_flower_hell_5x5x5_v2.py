from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

# Added modifications by Norbert Bátfai (nb4tf4i) batfai.norbert@inf.unideb.hu, mine.ly/nb4tf4i.1
# 2018.10.18, https://bhaxor.blog.hu/2018/10/18/malmo_minecraft
# 2020.02.02, NB4tf4i's Red Flowers, http://smartcity.inf.unideb.hu/~norbi/NB4tf4iRedFlowerHell


from builtins import range
import MalmoPython
import os
import sys
import time
import random
import json
import math

if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools

    print = functools.partial(print, flush=True)

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse(sys.argv)
except RuntimeError as e:
    print('ERROR:', e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)

# -- set up the mission -- #

missionXML_file = 'nb4tf4i_d5x5x5.xml'
with open(missionXML_file, 'r') as f:
    print("NB4tf4i's Red Flowers (Red Flower Hell) - DEAC-Hackers Battle Royale Arena\n")
    print("NB4tf4i vörös pipacsai (Vörös Pipacs Pokol) - DEAC-Hackers Battle Royale Arena\n\n")
    print(
        "The aim of this first challenge, called nb4tf4i's red flowers, is to collect as many red flowers as possible before the lava flows down the hillside.\n")
    print(
        "Ennek az első, az nb4tf4i vörös virágai nevű kihívásnak a célja összegyűjteni annyi piros virágot, amennyit csak lehet, mielőtt a láva lefolyik a hegyoldalon.\n")
    print("Norbert Bátfai, batfai.norbert@inf.unideb.hu, https://arato.inf.unideb.hu/batfai.norbert/\n\n")
    print("Loading mission from %s" % missionXML_file)
    mission_xml = f.read()
    my_mission = MalmoPython.MissionSpec(mission_xml, True)
    my_mission.drawBlock(0, 0, 0, "lava")


class Hourglass:
    def __init__(self, charSet):
        self.charSet = charSet
        self.index = 0

    def cursor(self):
        self.index = (self.index + 1) % len(self.charSet)
        return self.charSet[self.index]


hg = Hourglass('|/-\|')
# classon belul hogy? pls
turn_count = 0


class Steve:
    def __init__(self, agent_host):
        self.agent_host = agent_host
        self.x = 0
        self.y = 0
        self.z = 0
        self.x_old = 0
        self.y_old = 0
        self.z_old = 0
        self.yaw = 0
        self.pitch = 0
        self.lookingat = ""
        self.turncount = 0
        self.kettotle = False
        self.egyetle = False
        self.harmatle = False
        self.hotbar = True
        self.poppy=0
        self.poppycheck=0

    def simanfel(self, ut):
        self.agent_host.sendCommand("turn 1")
        self.agent_host.sendCommand("turn 1")
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")

        roadfel = 0
        while (roadfel < ut):
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("jumpmove 1")
            roadfel += 1
            time.sleep(.1)  # .2   .05
        # self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("turn -1")
        time.sleep(.01)  # .2

    def lavaig(self):
        lavatour = True
        """roadfel = 0
        while(roadfel < 48):
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("jumpmove 1")
            roadfel += 1
            """
        while lavatour:
            world_state = self.agent_host.getWorldState()
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                observations = json.loads(sensations)
                nbr5x5x5 = observations.get("nbr5x5", 0)
                # print("    3x3x3 neighborhood of Steve: ", nbr5x5x5)
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y = int(observations["YPos"])
                # print("    Steve's Coords: ", self.x, self.y, self.z)
                # print("    Steve's Yaw: ", self.yaw)
                # print("    Steve's Pitch: ", self.pitch)
                if ("flowing_lava" in nbr5x5x5):
                    lavatour = False
                    self.agent_host.sendCommand("move -1")
                    self.agent_host.sendCommand("move -1")
                    self.agent_host.sendCommand("turn 1")
            if (lavatour):
                self.agent_host.sendCommand("move 1")
                self.agent_host.sendCommand("jumpmove 1")
            time.sleep(.05)
        print("Lávát látok")

    def lavatol_vissza(self, emelet):
        self.agent_host.sendCommand("turn 1")
        akt_emelet = 0
        while akt_emelet < emelet:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
            akt_emelet += 1
        time.sleep(.1)
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("turn 1")
        print(self.yaw)
        time.sleep(.05)

    def is_lava(self, nbr5x5x5):
        if "flowing_lava" in nbr5x5x5:
            if(self.kettotle):
                self.szint_2()
            else:
                self.szint_1()

    def go_down(self):
        if self.harmatle and self.kettotle and self.egyetle:
            self.szint_3()
            self.turncount=0
        elif self.egyetle and self.kettotle:
            self.szint_2()
            self.turncount=0
        elif self.egyetle:
            self.szint_1()
            self.turncount = 0

    def szint_1(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        self.egyetle = self.kettotle
        self.kettotle = self.harmatle
        self.harmatle = False

    def szint_2(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        self.egyetle = self.harmatle
        self.kettotle = False
        self.harmatle = False

    def szint_3(self):
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        self.egyetle = False
        self.kettotle = False
        self.harmatle = False

    def hunavirag(self, le2, le1, l, lh1, lh2, e2, e1, a, h1, h2, je2, je1, j, jh1, jh2, nbr):
        if nbr[je2] == "red_flower":
            self.right_flower(2)
            self.go_down()
        if nbr[je1] == "red_flower":
            self.right_flower(1)
            self.go_down()
        if nbr[j] == "red_flower":
            self.right_flower(0)
            self.go_down()
        if nbr[jh1] == "red_flower":
            self.right_flower(-1)
            self.go_down()
        if nbr[jh2] == "red_flower":
            self.right_flower(-2)
            self.go_down()
        if nbr[e2] == "red_flower":
            self.center_flower(2)
            self.go_down()
            self.turncount = 0
        if nbr[e1] == "red_flower":
            self.center_flower(1)
            self.go_down()
            self.turncount = 0
        if nbr[a] == "red_flower":
            self.center_flower(0)
            self.go_down()
            self.turncount = 0
        if nbr[h1] == "red_flower":
            self.center_flower(-1)
            self.go_down()
            self.turncount = 0
        if nbr[h2] == "red_flower":
            self.center_flower(-2)
            self.go_down()
            self.turncount = 0
        if nbr[le2] == "red_flower":
            self.left_flower(2)
            self.go_down()
        if nbr[le1] == "red_flower":
            self.left_flower(1)
            self.go_down()
        if nbr[l] == "red_flower":
            self.left_flower(0)
            self.go_down()
        if nbr[lh1] == "red_flower":
            self.left_flower(-1)
            self.go_down()
        if nbr[lh2] == "red_flower":
            self.left_flower(-2)
            self.go_down()

    def is_flower(self, nbr):
        if (self.yaw == 0):
            self.hunavirag(99, 94, 89, 84, 79, 72, 67, 62, 57, 52, 45, 40, 35, 30, 25, nbr)
        elif (self.yaw == 90):
            self.hunavirag(45, 46, 47, 48, 49, 60, 61, 62, 63, 64, 75, 76, 77, 78, 79, nbr)
        elif (self.yaw == 180):
            self.hunavirag(25, 30, 35, 40, 45, 52, 57, 62, 67, 72, 79, 84, 89, 94, 99, nbr)
        else:
            self.hunavirag(29, 28, 27, 26, 25, 64, 63, 62, 61, 60, 99, 98, 97, 96, 95, nbr)

    def center_flower(self, mezo):
        print("x: "+str(self.x) + " old_ "+ str(self.x_old) +"     z:  "+str(self.z)+"  old_ "+str(self.z_old))

        """if(abs(self.x)==abs(self.x_old)-5 or abs(self.z) ==abs(self.z_old)-5):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif(abs(self.x)==abs(self.x_old)-4 or abs(self.z) == abs(self.z_old)-4):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif(abs(self.x)==abs(self.x_old)-3 or abs(self.z) == abs(self.z_old)-3):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 2 or abs(self.z) == abs(self.z_old) - 2):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 1 or abs(self.z) == abs(self.z_old) - 1):
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)"""
        if mezo == -2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        self.get_flower()
        self.kettotle = True
        if mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)

    def left_flower(self, mezo):
        print("x: "+str(self.x) + " old_ "+ str(self.x_old) +"     z:  "+str(self.z)+"  old_ "+str(self.z_old))

        """if (abs(self.x) == abs(self.x_old) - 5 or abs(self.z) == abs(self.z_old) - 5):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 4 or abs(self.z) == abs(self.z_old) - 4):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 3 or abs(self.z) == abs(self.z_old) - 3):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 2 or abs(self.z) == abs(self.z_old) - 2):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 1 or abs(self.z) == abs(self.z_old) - 1):
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)"""
        if mezo == -2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        elif mezo == -1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        self.agent_host.sendCommand("strafe -1")
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.05)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.05)  # .2
        self.agent_host.sendCommand("move 1")
        #time.sleep(.1)
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.harmatle = True
        if mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)

    def right_flower(self, mezo):
        print("x: "+str(self.x) + " old_ "+ str(self.x_old) +"     z:  "+str(self.z)+"  old_ "+str(self.z_old))
        """if (abs(self.x) == abs(self.x_old) - 5 or abs(self.z) == abs(self.z_old) - 5):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 4 or abs(self.z) == abs(self.z_old) - 4):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 3 or abs(self.z) == abs(self.z_old) - 3):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 2 or abs(self.z) == abs(self.z_old) - 2):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        elif (abs(self.x) == abs(self.x_old) - 1 or abs(self.z) == abs(self.z_old) - 1):
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)"""
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.05)
        self.agent_host.sendCommand("move 1")
        time.sleep(.05)
        if mezo == -2:
            self.agent_host.sendCommand("strafe 1")
            self.agent_host.sendCommand("strafe 1")
        elif mezo == -1:
            self.agent_host.sendCommand("strafe 1")
        elif mezo == 1:
            self.agent_host.sendCommand("strafe -1")
        elif mezo == 2:
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)
        self.get_flower()
        self.agent_host.sendCommand("move -1")
        self.agent_host.sendCommand("move -1")
        #time.sleep(.1)
        self.agent_host.sendCommand("turn -1")
        time.sleep(.01)
        self.egyetle = True
        if mezo == -2:
            self.agent_host.sendCommand("move 1")
            self.agent_host.sendCommand("move 1")
        elif mezo == -1:
            self.agent_host.sendCommand("move 1")
        elif mezo == 1:
            self.agent_host.sendCommand("move -1")
        elif mezo == 2:
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
        time.sleep(0.05)

    """
        
    def right_flower(self,nbr5x5x5):
        #if (nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)    #.05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_e1(self,nbr5x5x5):
        #if (nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)
        #self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)    #.05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False

    def right_flower_h1(self, nbr5x5x5):
        # if (nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.1)  # .05
        steve.get_flower()
        self.agent_host.sendCommand("strafe -1")
        if self.kettotle:
            # print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            # self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  # .1
            self.kettotle = False
        else:
            # print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  # .1
            self.kettotle = False

    def left_flower(self,nbr5x5x5):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)            #.1
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)      #.1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)    #.1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)      #.2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)     #.1
        self.kettotle=True

    def left_flower_e1(self, nbr5x5x5):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.1)  # .1
        #self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)  # .2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.kettotle = True

    def left_flower_h1(self, nbr5x5x5):
        if ((nbr5x5x5[10] != "dirt" and self.yaw == 180) or (nbr5x5x5[12] != "dirt" and self.yaw == 90) or (nbr5x5x5[14] != "dirt" and self.yaw == 270) or (nbr5x5x5[16] != "dirt" and self.yaw == 0)):
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("move -1")
            time.sleep(.05)  # .1
        self.agent_host.sendCommand("move -1")
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")
        time.sleep(.1)  # .1
        steve.get_flower()
        self.agent_host.sendCommand("turn 1")
        time.sleep(.05)  # .1
        self.agent_host.sendCommand("jumpmove 1")
        time.sleep(.1)  # .2
        self.agent_host.sendCommand("turn -1")
        time.sleep(.05)  # .1
        self.kettotle = True

    def alatta_flower(self, nbr5x5x5):
        steve.get_flower()
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False
    def elotte_flower(self, nbr5x5x5):
        self.agent_host.sendCommand("move 1")
        steve.get_flower()
        self.agent_host.sendCommand("move -1")
        if self.kettotle:
            #print("kettotle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            #self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)  #.1
            self.kettotle=False
        else:
            #print("egyetle")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            self.agent_host.sendCommand("move -1")
            self.agent_host.sendCommand("strafe -1")
            time.sleep(.05)    #.1
            self.kettotle=False
    """

    def is_wall(self, nbr5x5x5):
        world_state = self.agent_host.getWorldState()

        if (self.yaw == 0):
            if (nbr5x5x5[67] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    # self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.kettotle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0
                    # print("lentebb")

        elif (self.yaw == 270):
            if (nbr5x5x5[63] == "dirt"):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    # self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.harmatle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0

        elif (self.yaw == 180):
            if (nbr5x5x5[57] == "dirt" ):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    # self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.harmatle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0

        else:
            if (nbr5x5x5[61] == "dirt" ):  # elotte fal
                if (self.turncount <= 4 or self.harmatle == False):
                    # self.agent_host.sendCommand("move -1")
                    time.sleep(.1)
                    self.agent_host.sendCommand("turn -1")
                    time.sleep(.05)
                    self.turncount += 1
                else:
                    if self.harmatle:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    else:
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                        self.agent_host.sendCommand("move -1")
                        self.agent_host.sendCommand("strafe -1")
                    time.sleep(.1)
                    self.turncount = 0

    def get_flower(self):
        print("red_flower")
        self.agent_host.sendCommand("attack 1")
        time.sleep(.3)  # .5   #.3
        world_state = self.agent_host.getWorldState()
        sensations = world_state.observations[-1].text
        observations = json.loads(sensations)
        hotbar0 = ""
        hotbar1=""
        if "Hotbar_0_variant" in observations:
            hotbar0 = observations["Hotbar_0_variant"]
        #if"Hotbar_1_variant" in observations:
        #    hotbar1 = observations["Hotbar_1_variant"]
        # print(hotbar0)
        # print(hotbar1)
        if hotbar0 == "poppy" and self.hotbar:
            self.agent_host.sendCommand("hotbar.2 1")
            self.agent_host.sendCommand("hotbar.2 0")
            time.sleep(.1)
            self.hotbar = False
        else:
            if"Hotbar_1_size" in observations:
                self.poppy = observations["Hotbar_1_size"]
        self.poppycheck+=1
        if(self.poppycheck != self.poppy):
            world_state = self.agent_host.getWorldState()
            while world_state.number_of_observations_since_last_state == 0:
                world_state = self.agent_host.getWorldState()
                pass
            sensations = world_state.observations[-1].text
            # print("    sensations: ", sensations)
            observations = json.loads(sensations)
            nbr5x5x5 = observations.get("nbr5x5", 0)
            # print("\n    3x3x3 neighborhood of Steve: \n", nbr5x5x5)

            if "Yaw" in observations:
                self.yaw = int(observations["Yaw"])
            if "Pitch" in observations:
                self.pitch = int(observations["Pitch"])
            if "XPos" in observations:
                self.x = int(observations["XPos"])
            if "ZPos" in observations:
                self.z = int(observations["ZPos"])
            if "YPos" in observations:
                self.y = int(observations["YPos"])
            self.is_flower(nbr5x5x5)
        # time.sleep(.05)  #.1
        self.agent_host.sendCommand("jumpuse")
        time.sleep(.1)  # .2
        # else:
        # time.sleep(.1)
        """self.agent_host.sendCommand("strafe -1")  # visszamegy az elozo helyere
        time.sleep(.1)
        self.agent_host.sendCommand("strafe -1")  # 1 szinttel lentebb
        self.agent_host.sendCommand("move -1")  # sarok miatt
        self.agent_host.sendCommand("move -1")  #
        """

        time.sleep(.1)

    def move(self, nbr5x5x5):

        steve.is_lava(nbr5x5x5)
        steve.is_flower(nbr5x5x5)
        steve.is_wall(nbr5x5x5)
        """if(self.y==4 or self.y==3):
            self.agent_host.sendCommand("move 1")
        else:"""
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")
        self.agent_host.sendCommand("move 1")
        time.sleep(.1)
        world_state = self.agent_host.getWorldState()



    def run(self):
        world_state = self.agent_host.getWorldState()
        # world_state = self.agent_host.peekWorldState()
        self.agent_host.sendCommand("look 1")
        self.agent_host.sendCommand("look 1")
        self.hotbar = True
        # Loop until mission ends:
        # steve.simanfel(60)  #35-->31    40-->37    43nal meghal!! #41-->37    !!!!!!47!!!!   #50 --> 46
        steve.lavaig()
        steve.lavatol_vissza(10)  # 33-->23
        self.agent_host.sendCommand("strafe 1")
        time.sleep(.05)
        while world_state.is_mission_running:
            if world_state.number_of_observations_since_last_state != 0:
                sensations = world_state.observations[-1].text
                # print("    sensations: ", sensations)
                observations = json.loads(sensations)
                nbr5x5x5 = observations.get("nbr5x5", 0)
                #print("\n    3x3x3 neighborhood of Steve: \n", nbr5x5x5)

                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x_old = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z_old = int(observations["ZPos"])
                if "YPos" in observations:
                    self.y_old = int(observations["YPos"])
                # print(self.yaw)
                steve.move(nbr5x5x5)
            # else:
            # print("MOREEBUUBAJLESZ")

            world_state = self.agent_host.getWorldState()


num_repeats = 1
for ii in range(num_repeats):

    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 6
    for retry in range(max_retries):
        try:
            agent_host.startMission(my_mission, my_mission_record)
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                print("Attempting to start the mission:")
                time.sleep(2)

    # Loop until mission starts:
    print("   Waiting for the mission to start ")
    world_state = agent_host.getWorldState()

    while not world_state.has_mission_begun:
        print("\r" + hg.cursor(), end="")
        time.sleep(0.15)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:", error.text)

    print("NB4tf4i Red Flower Hell running\n")
    steve = Steve(agent_host)
    steve.run()

print("Mission ended")
# Mission has ended.
