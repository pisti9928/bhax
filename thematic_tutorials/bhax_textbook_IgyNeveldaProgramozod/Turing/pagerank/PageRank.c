#include <stdio.h>
#include <math.h>
#include <iostream>
static void Kiir (double tomb[], int db)
{
   for(int i=0;i<4;i++)
   {
        printf("%f  \n", tomb[i]);
   }
}
double tavolsag (double PR[], double PRv[], int n)
{
    double osszeg=0.0;
    for (int i=0; i<n; i++)
    {
        osszeg+= (PRv[i]-PR[i])*(PRv[i]-PR[i]);
    }
    return sqrt(osszeg);
}
int main()
{
    int n=4;
    double L[n][n] = {
    {0.0,     0.0  , 1.0 / 3.0, 0.0},
    {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
    {0.0, 1.0 / 2.0,    0.0   , 0.0},
    {0.0,    0.0   , 1.0 / 3.0, 0.0}
    };
    double PR[4] = {0.0, 0.0, 0.0, 0.0};
    double PRv[4] = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};

    

    for (;;)
    {
        for(int i=0;i<n;i++)
        {
            PR[i]=0;
            for(int j=0; j<n;j++)
            {
                PR[i] += L[i][j] * PRv[j];
            }
        }
        if(tavolsag(PR,PRv,n)<0.0000000001)
        {
            break;
        }
        for(int i=0;i<n;i++)
        {
            PRv[i]=PR[i];
        }
    }
    Kiir(PR,n);

    return 0;
}
