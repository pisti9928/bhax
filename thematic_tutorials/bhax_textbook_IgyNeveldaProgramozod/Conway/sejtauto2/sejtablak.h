#ifndef SEJTABLAK_H
#define SEJTABLAK_H

#include <QMainWindow>
#include <QImage>
#include <QPainter>
#include <QPixmap>
#include <QWidget>
#include <QMouseEvent>
#include <QDebug>
#include "sejtszal.h"

class SejtSzal;

class SejtAblak : public QMainWindow
{
  Q_OBJECT
  
public:
  SejtAblak(int szelesseg = 100, int magassag = 75, QWidget *parent = 0);
  ~SejtAblak();
  // Egy sejt lehet �l�
  static const bool ELO = true;
  // vagy halott
  static const bool HALOTT = false;
  void vissza(int racsIndex);
  int varakozas;
protected:
  // K�t r�csot haszn�lunk majd, az egyik a sejtt�r �llapot�t
  // a t_n, a m�sik a t_n+1 id�pillanatban jellemzi.
  bool ***racsok;
  // Valamelyik r�csra mutat, technikai jelleg�, hogy ne kelljen a
  // [2][][]-b�l az els� dimenzi�t haszn�lni, mert vagy az egyikre
  // �ll�tjuk, vagy a m�sikra.
  bool **racs;
  // Megmutatja melyik r�cs az aktu�lis: [r�csIndex][][]
  int racsIndex;
  // Pixelben egy cella adatai.
  int cellaSzelesseg;
  int cellaMagassag;
  // A sejtt�r nagys�ga, azaz h�nyszor h�ny cella van?
  int szelesseg;
  int magassag;    
  void paintEvent(QPaintEvent*);
  void siklo(bool **racs, int x, int y);
  void sikloKilovo(bool **racs, int x, int y);
  void mousePressEvent(QMouseEvent*);
  void mouseMoveEvent(QMouseEvent*);
  void mouseReleaseEvent(QMouseEvent*);
  void keyPressEvent(QKeyEvent*);
  int mx;
  int my;

  bool mouse_pressed;
  
private:
  SejtSzal* eletjatek;
  
};

#endif // SEJTABLAK_H
