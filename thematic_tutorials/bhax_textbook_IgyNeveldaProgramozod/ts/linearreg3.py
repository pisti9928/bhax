import numpy
import matplotlib.pyplot
import tensorflow
import tensorflow.compat.v1 as tensorflow
tensorflow.disable_v2_behavior()

x_data = numpy.random.uniform(-1, 1, 10000)
e_data = numpy.random.normal(1, .05, 10000)

y_data = .3 * x_data + .1 + e_data

b = tensorflow.Variable(.0, name="b")
m = tensorflow.Variable(.0, name="m")

x = tensorflow.convert_to_tensor(x_data, tensorflow.float32)
y = tensorflow.convert_to_tensor(y_data, tensorflow.float32)

opt = tensorflow.optimizers.SGD(.008)

def train():
    with tensorflow.GradientTape() as tape:
        # y = mx + b
        lin_model = tensorflow.add(tensorflow.multiply(m, x), b)
        loss = tensorflow.reduce_mean(tensorflow.square(lin_model - y), name="error")
        grads = tape.gradient(loss, [m, b])
        opt.apply_gradients(zip(grads, [m,b]))

i = 0
prev = 0
while True:
    train()

    print(m.numpy(), b.numpy())

    #matplotlib.pyplot.figure()
    #matplotlib.pyplot.plot(x_data, y_data, "r^")
    #matplotlib.pyplot.plot(x_data, sess.run(m) * x_data + b.numpy())
    #matplotlib.pyplot.savefig("linearreg"+str(i)+".png")
    i = i + 1

    if abs(prev - m.numpy()) < .000001 and i > 1000 :
        break
    prev = m.numpy()

matplotlib.pyplot.figure()
matplotlib.pyplot.plot(x_data, y_data, "r^")
matplotlib.pyplot.plot(x_data, sess.run(m) * x_data + b.numpy())
matplotlib.pyplot.savefig("linearreg"+str(i)+".png")