import numpy
import matplotlib.pyplot
import tensorflow
import tensorflow.compat.v1 as tensorflow
tensorflow.disable_v2_behavior()

x_data = numpy.random.uniform(-1, 1, 10)
e_data = numpy.random.normal(1, .05, 10)

y_data = .3 * x_data + .1 + e_data

b = tensorflow.Variable(.0, name="b")
m = tensorflow.Variable(.0, name="m")

x = tensorflow.placeholder(tensorflow.float32, [10], name="x_data")
y = tensorflow.placeholder(tensorflow.float32, [10], name="y_data")

# y = mx + b

lin_model = tensorflow.add(tensorflow.multiply(m, x), b)

loss = tensorflow.reduce_mean(tensorflow.square(lin_model - y), name="error")
opt = tensorflow.train.GradientDescentOptimizer(.5)
train = opt.minimize(loss)

init = tensorflow.global_variables_initializer()
sess = tensorflow.Session()
sess.run(init)

i = 0
prev = 0
while True:
    sess.run(train, {x: x_data, y: y_data})

    print(sess.run(m), sess.run(b))

    matplotlib.pyplot.figure()
    matplotlib.pyplot.plot(x_data, y_data, "r^")
    matplotlib.pyplot.plot(x_data, sess.run(m) * x_data + sess.run(b))

    matplotlib.pyplot.savefig("linearreg"+str(i)+".png")
    i = i + 1

    if abs(prev - sess.run(m)) < .001:
        break
    prev = sess.run(m)

tensorflow.summary.FileWriter("/tmp/lr", sess.graph)


#matplotlib.pyplot.figure()
#matplotlib.pyplot.plot(x_data, y_data, "r^")
#matplotlib.pyplot.show()